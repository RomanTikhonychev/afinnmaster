//
//  TRIndexManager.m
//  VigenereMaster
//
//  Created by Рома on 2/26/16.
//  Copyright © 2016 Roman Tikhonychev. All rights reserved.
//

#import "TRIndexManager.h"
#import "TRFrequencyAnalyzer.h"

#define mod %

static const NSInteger singleLetter = 1;


@implementation TRIndexManager


+ (NSArray *)spreadText:(NSString *)text inToBlocksWithPeriod:(NSInteger)period
{
    NSMutableArray *outputArray = [NSMutableArray new];
    
    
    for (NSInteger i = 0; i < period; i++) {
        NSMutableString *blockOfText = [[NSMutableString alloc]initWithString:@""];
        [outputArray addObject:blockOfText];
    }
    
    
    for (NSInteger i = 0; i < [text length]; i++) {
        
        NSInteger blockArrayIndex = i mod period;
        NSString *letter = [text substringWithRange:NSMakeRange(i, singleLetter)];
        NSMutableString *currentBlock = [outputArray objectAtIndex:blockArrayIndex];
    
        [currentBlock appendString:letter];
    }
    
    return outputArray;
}


+ (CGFloat)countIndexOfAccordanceForBlockOfText:(NSString *)text withAplhabet:(NSString *)aplhabet
{
    NSDictionary *letterFrequencyDictionary = [TRFrequencyAnalyzer calculateLetterFrequencyForText:text wihtAlphapet:aplhabet];
    
    NSArray *alphabetLetters             = [letterFrequencyDictionary objectForKey:@"alphabetLetters"];
    NSArray *alphabetLettersFrequency    = [letterFrequencyDictionary objectForKey:@"alphabetLettersFrequency"];
    
    
    CGFloat indexOfAccordance = 0;
    CGFloat textLenght = [text length];
    
    for (NSInteger i = 0; i < [alphabetLetters count]; i++) {
        
        CGFloat summElement = [alphabetLettersFrequency[i] floatValue] * ([alphabetLettersFrequency[i] floatValue] - 1);
        
        if (isnan(summElement)) {
            summElement = 0;
        }
        
        indexOfAccordance = indexOfAccordance + summElement;
    }
    
    return (1/(textLenght * (textLenght - 1)))*indexOfAccordance;
}


+ (NSArray *)calculateIndexsOfAccordancesForText:(NSString *)text usingAlphabet:(NSString *)alphabet withSMALLPeriod:(NSInteger)period
{
    
    NSArray *spreatedText = [TRIndexManager spreadText:text inToBlocksWithPeriod:period];
    
    NSMutableArray *textIndexes = [NSMutableArray new];
    
    for (NSString *blockOfText in spreatedText) {
        
        CGFloat currentIndex = [TRIndexManager countIndexOfAccordanceForBlockOfText:blockOfText withAplhabet:alphabet];
        NSString *currentIndexString = [NSString stringWithFormat:@"%f",currentIndex];
        [textIndexes addObject:currentIndexString];
    }
    
    return textIndexes;
}


+ (NSInteger)calculateStatisticMatchForText:(NSString *)text usingAlphabet:(NSString *)alphabet withBIGPeriod:(NSInteger)period
{
    NSInteger textLenght = [text length];
    
    NSInteger statisticMatchValue = 0;
    
    for (NSInteger i = 0; i < (textLenght - period); i++) {
        
        NSString *firstCompareletter  = [text substringWithRange:NSMakeRange(i, 1)];
        NSString *secondCompareletter = [text substringWithRange:NSMakeRange(i + period, 1)];
        
        
        if ([firstCompareletter isEqualToString:secondCompareletter]) {
            statisticMatchValue++;
        }
        
    }
    
    return statisticMatchValue;
}


//- (int)searchDFunction:(char *)text;
//{
//    int r = 6;          // 6 например
//    int match = 0;      // количество повторений, изначально 0
//    
//    int textLenght = 1000000; //длина твоего текста
//    
//    for (int i = 0; i < textLenght - r; i++) {
//        
//        char firstCompareSymbol  = text[i];
//        char secondCompareSymbol = text[i + r];
//        
//        if (firstCompareSymbol == secondCompareSymbol) {
//            match++;
//        }
//    }
//    
//    return match;
//}


@end













