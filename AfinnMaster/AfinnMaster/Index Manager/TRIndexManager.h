//
//  TRIndexManager.h
//  VigenereMaster
//
//  Created by Рома on 2/26/16.
//  Copyright © 2016 Roman Tikhonychev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TRIndexManager : NSObject

+ (NSArray *)spreadText:(NSString *)text inToBlocksWithPeriod:(NSInteger)period;

+ (CGFloat)countIndexOfAccordanceForBlockOfText:(NSString *)text withAplhabet:(NSString *)aplhabet;

+ (NSArray *)calculateIndexsOfAccordancesForText:(NSString *)text usingAlphabet:(NSString *)alphabet withSMALLPeriod:(NSInteger)period;

+ (NSInteger)calculateStatisticMatchForText:(NSString *)text usingAlphabet:(NSString *)alphabet withBIGPeriod:(NSInteger)period;

@end
