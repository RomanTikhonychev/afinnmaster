//
//  main.m
//  AfinnMaster
//
//  Created by Рома on 3/13/16.
//  Copyright © 2016 Roman Tikhonychev. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
