//
//  AppDelegate.m
//  AfinnMaster
//
//  Created by Рома on 3/13/16.
//  Copyright © 2016 Roman Tikhonychev. All rights reserved.
//

#import "AppDelegate.h"

#import "SRMMaster.h"
#import "TRTextCleaner.h"
#import "TextRecognizer.h"
#import "TRFrequencyAnalyzer.h"
#import "AfinnEncryptingManager.h"
#import "TRIndexManager.h"

#define toString        [NSString alloc] initWithFormat:@"%i"
#define RussianAlphabetEEHardSign @"абвгдежзийклмнопрстуфхцчшщыьэюя"


@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    
#pragma mark - SRMMaster check

//    NSString *result0 = [SRMMaster getConversedValueFor:15 withModul:37];
//    NSString *result1 = [SRMMaster solveLinearComparisonForValueA:111 valueB:75 valueN:321];

    
#pragma mark - Frequency Analyzer check

    
//    NSDictionary *result4 = [TRFrequencyAnalyzer calculateBigrammaFrequencyANDReturnDictionaryForText:@"ааббааббаааасс" wihtAlphapet:RussianAlphabetEE];
//
//    NSArray *result41 = [result4 objectForKey:@"bigrammasArray"];
//    NSArray *resulr42 = [result4 objectForKey:@"bigrammasFrequencyArray"];
//    
//    for (int index = 0; index < [result41 count]; index++) {
//        
//        NSLog(@"Bigramma: %@ Counter %@\n", result41[index], resulr42[index]);
//    }
 
#pragma mark - found frequency for 17 variant
    
//    NSString* path = [[NSBundle mainBundle] pathForResource:@"17"
//                                                     ofType:@"txt"];
//    
//    NSError *error = [[NSError alloc] init];
//    NSString* content = [NSString stringWithContentsOfFile:path
//                                                  encoding:NSUnicodeStringEncoding
//                                                     error:&error];
//    
//    NSString *cleaneText = [TRTextCleaner cleaneText:content andLeaveNextCharacters:RussianAlphabetEE];
//    
//    NSDictionary *result5 = [TRFrequencyAnalyzer calculateBigrammaFrequencyANDReturnDictionaryForText:cleaneText
//                                                                                         wihtAlphapet:RussianAlphabetEE];
//   
//    NSArray *max6Values             = [result5 objectForKey:@"max6Values"];
//    NSArray *max6ValuesBigrammas    = [result5 objectForKey:@"max6ValuesBigrammas"];
//    
//    for (int i = 0; i < [max6Values count]; i++) {
//        NSLog(@"biggramma: %@, frequency: %@", max6ValuesBigrammas[i], max6Values[i]);
//    }
    
#pragma mark - AfinnEncryptingManager check
    
    //int x = [AfinnEncryptingManager countNumberForBigramm:@"аа"];
    
//    [AfinnEncryptingManager tryToGuessKeyExpectingThatMostPopularBigramm:@"но"
//                                                              cypheredAs:@"пр"
//                                                           secondPopular:@"ст"
//                                                              cypheredAs:@"кс"];
    
//    NSString *result7 = [AfinnEncryptingManager encryptText:@"гуиг" withKeyA:592 withKeyB:713 wihtAlphapet:@"абвгґдеєжзиіїйклмнопрстуфхцчшщьюя"];
    
#pragma mark - Get keys for 17
    
//    NSString* path = [[NSBundle mainBundle] pathForResource:@"17"
//                                                     ofType:@"txt"];
//
//    NSError *error = [[NSError alloc] init];
//    NSString* content = [NSString stringWithContentsOfFile:path
//                                                  encoding:NSUnicodeStringEncoding
//                                                     error:&error];
//
//    NSString *cleaneText = [TRTextCleaner cleaneText:content andLeaveNextCharacters:RussianAlphabetEE];

//    NSDictionary *result5 = [TRFrequencyAnalyzer calculateBigrammaFrequencyANDReturnDictionaryForText:cleaneText
//                                                                                         wihtAlphapet:RussianAlphabetEE];
//
//    NSArray *max6Values             = [result5 objectForKey:@"max6Values"];
//    NSArray *max6ValuesBigrammas    = [result5 objectForKey:@"max6ValuesBigrammas"];
//
//    for (int i = 0; i < [max6Values count]; i++) {
//        NSLog(@"biggramma: %@, frequency: %@", max6ValuesBigrammas[i], max6Values[i]);
//    }
//    
    /*
     016-03-14 04:28:26.109 AfinnMaster[11644:170116]  biggramma: вк, frequency: 179
     2016-03-14 04:28:26.109 AfinnMaster[11644:170116] biggramma: нв, frequency: 144
     2016-03-14 04:28:26.109 AfinnMaster[11644:170116] biggramma: ья, frequency: 140
     2016-03-14 04:28:26.109 AfinnMaster[11644:170116] biggramma: юв, frequency: 136
     2016-03-14 04:28:26.109 AfinnMaster[11644:170116] biggramma: пк, frequency: 123
     2016-03-14 04:28:26.109 AfinnMaster[11644:170116] biggramma: юе, frequency: 116
     */
    
//    NSArray *openBigramms       = @[@"ст", @"но", @"то", @"на", @"ен"];
//    NSArray *cypheredBigramms   = @[@"вк", @"нв", @"ья", @"юв", @"юе"];
//    
//    int counter = 0;
//
//    
//    for (int open1 = 0; open1 < [openBigramms count]; open1 ++) {
//        for (int open2 = 0; open2 < [openBigramms count]; open2++) {
//            
//            if (open1 == open2) {
//                
//            } else {
//
//            
//                    for (int close1 = 0; close1 < [openBigramms count]; close1++) {
//                        for (int close2 = 0; close2 < [openBigramms count]; close2++) {
//                            
//                            if (close1 == close2) {
//                                
//                            } else {
//                    
//                            
//                                    NSArray *keyArray = [AfinnEncryptingManager tryToGuessKeyExpectingThatMostPopularBigramm:openBigramms[open1]
//                                                                                                                      cypheredAs:cypheredBigramms[close1]
//                                                                                                                   secondPopular:openBigramms[open2]
//                                                                                                                      cypheredAs:cypheredBigramms[close2]];
//                                
//                                //а = 470 в = 312
//                                for (NSDictionary *keyAB in keyArray)
//                                {
//                                    int a = (int)[[keyAB objectForKey:@"keyA"] integerValue];
//                                    int b = (int)[[keyAB objectForKey:@"keyB"] integerValue];
//                                    
//                                    NSString *encryptedText = [AfinnEncryptingManager encryptText:cleaneText withKeyA:a withKeyB:b wihtAlphapet:RussianAlphabetEEHardSign];
//                                    
//                                    
//                                    
//                                    BOOL russian = [TextRecognizer isRussianLanguageText:encryptedText];
//                                    
//                                    if (russian) {
//                                        NSLog(@"here?");
//                                    }
//                                    
//                                    counter++;
//                                    NSLog(@"NEXT KEY counter %i\n\n",counter);
//                                }
//                                
//                            }
//                            
//                        }
//                    }
//            }
//        }
//    }

#pragma mark - Valera
    
    NSString* path = [[NSBundle mainBundle] pathForResource:@"7"
                                                     ofType:@"txt"];
    
    NSError *error = [[NSError alloc] init];
    NSString* content = [NSString stringWithContentsOfFile:path
                                                  encoding:NSUnicodeStringEncoding
                                                     error:&error];
    
    NSString *cleaneText = [TRTextCleaner cleaneText:content andLeaveNextCharacters:RussianAlphabetEE];
    
        NSDictionary *result5 = [TRFrequencyAnalyzer calculateBigrammaFrequencyANDReturnDictionaryForText:cleaneText
                                                                                             wihtAlphapet:RussianAlphabetEE];
    
        NSArray *max6Values             = [result5 objectForKey:@"max6Values"];
        NSArray *max6ValuesBigrammas    = [result5 objectForKey:@"max6ValuesBigrammas"];
    
        for (int i = 0; i < [max6Values count]; i++) {
            NSLog(@"biggramma: %@, frequency: %@", max6ValuesBigrammas[i], max6Values[i]);
        }
    
    
    
    
//    NSArray *openBigramms       = @[@"ст", @"но", @"то", @"на", @"ен"];
//    NSArray *cypheredBigramms   = @[@"еш", @"еы", @"ск", @"шя", @"до"];
//    
//    
//    
//    int counter = 0;
//
//
//    for (int open1 = 0; open1 < [openBigramms count]; open1 ++) {
//        for (int open2 = 0; open2 < [openBigramms count]; open2++) {
//
//            if (open1 == open2) {
//
//            } else {
//
//
//                    for (int close1 = 0; close1 < [openBigramms count]; close1++) {
//                        for (int close2 = 0; close2 < [openBigramms count]; close2++) {
//
//                            if (close1 == close2) {
//
//                            } else {
//
//
//                                    NSArray *keyArray = [AfinnEncryptingManager tryToGuessKeyExpectingThatMostPopularBigramm:openBigramms[open1]
//                                                                                                                      cypheredAs:cypheredBigramms[close1]
//                                                                                                                   secondPopular:openBigramms[open2]
//                                                                                                                      cypheredAs:cypheredBigramms[close2]];
//
//                                //а = 470 в = 312
//                                for (NSDictionary *keyAB in keyArray)
//                                {
//                                    int a = (int)[[keyAB objectForKey:@"keyA"] integerValue];
//                                    int b = (int)[[keyAB objectForKey:@"keyB"] integerValue];
//
//                                    NSString *encryptedText = [AfinnEncryptingManager encryptText:cleaneText withKeyA:a withKeyB:b wihtAlphapet:RussianAlphabetEEHardSign];
//
//
//
//                                    BOOL russian = [TextRecognizer isRussianLanguageText:encryptedText];
////а = 10 б = 390
//                                    if (russian) {
//                                        NSLog(@"here?");
//                                    }
//
//                                    counter++;
//                                    NSLog(@"NEXT KEY counter %i\n\n",counter);
//                                }
//
//                            }
//                            
//                        }
//                    }
//            }
//        }
//    }

    
#pragma mark - run all variants
    
//    NSMutableArray *popularBigrammsInAllVariant = [NSMutableArray new];
//    NSMutableArray *keysForAllVariants = [NSMutableArray new];
//    
//    for (int textIndex = 0; textIndex < 25; textIndex++) {
//        
//        
//        NSString* path = [[NSBundle mainBundle] pathForResource:[[NSString alloc] initWithFormat:@"%i", textIndex + 1]
//                                                         ofType:@"txt"];
//        
//        NSError *error = [[NSError alloc] init];
//        NSString* content = [NSString stringWithContentsOfFile:path
//                                                      encoding:NSUnicodeStringEncoding
//                                                         error:&error];
//        NSString *cleaneText = [TRTextCleaner cleaneText:content andLeaveNextCharacters:RussianAlphabetEE];
//        
//        
//                NSDictionary *result5 = [TRFrequencyAnalyzer calculateBigrammaFrequencyANDReturnDictionaryForText:cleaneText
//                                                                                                     wihtAlphapet:RussianAlphabetEE];
//        
//                NSArray *max6Values             = [result5 objectForKey:@"max6Values"];
//                NSArray *max6ValuesBigrammas    = [result5 objectForKey:@"max6ValuesBigrammas"];
//        
////                for (int i = 0; i < [max6Values count]; i++) {
////                    NSLog(@"biggramma: %@, frequency: %@", max6ValuesBigrammas[i], max6Values[i]);
////                }
//        
//        [popularBigrammsInAllVariant addObject:max6ValuesBigrammas];
//        
//        NSArray *openBigramms = @[@"ст", @"но", @"то", @"на", @"ен"];
//        
//        
//        
//        //-----
//        
//            int counter = 0;
//        
//        
//            for (int open1 = 0; open1 < [openBigramms count]; open1 ++) {
//                for (int open2 = 0; open2 < [openBigramms count]; open2++) {
//        
//                    if (open1 == open2) {
//        
//                    } else {
//        
//        
//                            for (int close1 = 0; close1 < [openBigramms count]; close1++) {
//                                for (int close2 = 0; close2 < [openBigramms count]; close2++) {
//        
//                                    if (close1 == close2) {
//        
//                                    } else {
//        
//        
//                                            NSArray *keyArray = [AfinnEncryptingManager tryToGuessKeyExpectingThatMostPopularBigramm:openBigramms[open1]
//                                                                                                                              cypheredAs:max6ValuesBigrammas[close1]
//                                                                                                                           secondPopular:openBigramms[open2]
//                                                                                                                              cypheredAs:max6ValuesBigrammas[close2]];
//        
//                                        //а = 470 в = 312
//                                        for (NSDictionary *keyAB in keyArray)
//                                        {
//                                            int a = (int)[[keyAB objectForKey:@"keyA"] integerValue];
//                                            int b = (int)[[keyAB objectForKey:@"keyB"] integerValue];
//        
//                                            NSString *encryptedText = [AfinnEncryptingManager encryptText:cleaneText withKeyA:a withKeyB:b wihtAlphapet:RussianAlphabetEEHardSign];
//        
//        
//        
//                                            BOOL russian = [TextRecognizer isRussianLanguageText:encryptedText];
//        //а = 10 б = 390
//                                            if (russian) {
//                                                //NSLog(@"here?");
//                                                
//                                                NSString *keyString = [[NSString alloc] initWithFormat:@"variant = %i, a = %i, b = %i", textIndex + 1, a, b];
//                                                [keysForAllVariants addObject:keyString];
//                                                NSLog(keyString);
//                                            }
//        
//                                            counter++;
//                                           // NSLog(@"NEXT KEY counter %i\n\n",counter);
//                                        }
//        
//                                    }
//                                    
//                                }
//                            }
//                    }
//                }
//            }
//        
//        //-----
//        
//    }
    
}



- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

@end
