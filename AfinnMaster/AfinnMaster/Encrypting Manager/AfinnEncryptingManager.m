//
//  AfinnEncryptingManager.m
//  AfinnMaster
//
//  Created by Рома on 3/14/16.
//  Copyright © 2016 Roman Tikhonychev. All rights reserved.
//

#import "AfinnEncryptingManager.h"
#import "SRMMaster.h"

#define RussianAlphabetEEHardSign @"абвгдежзийклмнопрстуфхцчшщыьэюя"
#define mod %
#define toString        [NSString alloc] initWithFormat:@"%i",


@implementation AfinnEncryptingManager

+ (NSArray *)tryToGuessKeyExpectingThatMostPopularBigramm:(NSString *)firstBigramm
                                          cypheredAs:(NSString *)cypheredFirstBigramm
                                       secondPopular:(NSString *)secondBigramm
                                          cypheredAs:(NSString *)cypheredsecondBigramm
{
    int x1 = [AfinnEncryptingManager countNumberForBigramm:firstBigramm usingAplhabet:RussianAlphabetEEHardSign];
    int x2 = [AfinnEncryptingManager countNumberForBigramm:secondBigramm usingAplhabet:RussianAlphabetEEHardSign];
    
    int y1 = [AfinnEncryptingManager countNumberForBigramm:cypheredFirstBigramm usingAplhabet:RussianAlphabetEEHardSign];
    int y2 = [AfinnEncryptingManager countNumberForBigramm:cypheredsecondBigramm usingAplhabet:RussianAlphabetEEHardSign];
    
    int n2 = (int)pow(((int)[RussianAlphabetEEHardSign length]), 2);

    //create linear ax = b (mod m^2)
    int b = y1 - y2;
    int a = x1 - x2;
    
    while (a < 0) {
        a += n2;
    }
    while (b < 0) {
        b += n2;
    }
    
    
    NSString *keyA = [SRMMaster solveLinearComparisonForValueA:a valueB:b valueN:n2];
    
    
    
    
    NSArray *elements = [keyA componentsSeparatedByString:@","];
    NSMutableArray *outputDictionaries = [NSMutableArray new];
    
    
//    if ([elements count] > 1) {
//        NSLog(@"manyAnswers");
//    }
    
    for (NSString *keyAonOF in elements) {
        
        int intKeyA = (int)[keyAonOF integerValue];
        int intKeyB = ((y1 - intKeyA*x1) mod n2);
        
        while (intKeyB < 0) {
            intKeyB += n2;
        }
        
        NSDictionary *result = @{@"keyA" : [toString intKeyA],
                                 @"keyB" : [toString intKeyB]};
        
        
        [outputDictionaries addObject:result];
    }
    
    
    return outputDictionaries;
}

+ (int)countNumberForBigramm:(NSString *)bigramm usingAplhabet:(NSString *)alphabet
{
    int x1 = -1, x2 = -1;
    int n2 = (int)pow(((int)[alphabet length]), 2);
    int n = (int)[alphabet length];
    
    for (int i = 0; i < [alphabet length]; i++) {
        
        if ([bigramm characterAtIndex:0] == [alphabet characterAtIndex:i]) {
            x1 = i;
        }
        if ([bigramm characterAtIndex:1] == [alphabet characterAtIndex:i]) {
            x2 = i;
        }
    }
    
    
    int output = (x1*n + x2) mod n2;
    
    return output;
}


+ (NSString *)encryptText:(NSString *)text
                 withKeyA:(NSInteger)keyA
                 withKeyB:(NSInteger)keyB
             wihtAlphapet:(NSString *)alphabet
{
    NSMutableString *outputText = [[NSMutableString alloc] initWithString:@""];
    int n2 = (int)pow(((int)[alphabet length]), 2);
    int n = ((int)[alphabet length]);
    
    for (int index = 0; index < [text length]; index += 2) {
        
        NSString *bigramm = [text substringWithRange:NSMakeRange(index, 2)];
        int yi = [AfinnEncryptingManager countNumberForBigramm:bigramm usingAplhabet:alphabet];
        
        int keyAConversed = (int)[[SRMMaster getConversedValueFor:(int)keyA withModul:n2] integerValue];
        
        int xi = (keyAConversed * (yi - keyB)) mod n2;
        while (xi < 0) {
            xi += n2;
        }
    
        int x2 = xi mod n;
        float x1 = (xi - x2)/n;
        
        NSString *decypheredBigramma = [[NSString alloc] initWithFormat:@"%@%@",
                                        [alphabet substringWithRange:NSMakeRange(x1, 1)],
                                        [alphabet substringWithRange:NSMakeRange(x2, 1)]];
        
        [outputText appendString:decypheredBigramma];
    }
    
    return outputText;
}

@end
