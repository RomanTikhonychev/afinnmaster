//
//  AfinnEncryptingManager.h
//  AfinnMaster
//
//  Created by Рома on 3/14/16.
//  Copyright © 2016 Roman Tikhonychev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AfinnEncryptingManager : NSObject

+ (NSArray *)tryToGuessKeyExpectingThatMostPopularBigramm:(NSString *)firstBigramm
                                          cypheredAs:(NSString *)cypheredFirstBigramm
                                       secondPopular:(NSString *)secondBigramm
                                          cypheredAs:(NSString *)cypheredsecondBigramm;

+ (int)countNumberForBigramm:(NSString *)bigramm usingAplhabet:(NSString *)alphabet;

+ (NSString *)encryptText:(NSString *)text
                 withKeyA:(NSInteger)keyA
                 withKeyB:(NSInteger)keyB
             wihtAlphapet:(NSString *)alphabet;
@end
