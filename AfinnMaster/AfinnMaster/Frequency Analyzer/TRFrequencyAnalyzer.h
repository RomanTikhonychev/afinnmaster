//
//  TRFrequencyAnalyzer.h
//  VigenereMaster
//
//  Created by Рома on 2/23/16.
//  Copyright © 2016 Roman Tikhonychev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

#define RussianAlphabet @"абвгдеёжзийклмнопрстуфхцчшщъыьэюя"
#define RussianAlphabetWithWhitespace @"абвгдеёжзийклмнопрстуфхцчшщъыьэюя "
#define RussianAlphabetEE @"абвгдежзийклмнопрстуфхцчшщъыьэюя"


@interface TRFrequencyAnalyzer : NSObject

+ (NSDictionary *)calculateLetterFrequencyForText:(NSString *)text
                                     wihtAlphapet:(NSString *)alphabet;

+ (int **)calculateBigrammaFrequencyForText:(NSString *)text
                                       wihtAlphapet:(NSString *)alphabet;

+ (NSDictionary *)calculateBigrammaFrequencyANDReturnDictionaryForText:(NSString *)text
                                                          wihtAlphapet:(NSString *)alphabet;


+ (CGFloat)calculateLetterEntropyForText:(NSString *)text
                            wihtAlphapet:(NSString *)alphabet;

+ (CGFloat)calculateBigrammaEntropyForText:(NSString *)text
                              wihtAlphapet:(NSString *)alphabet;


+ (NSInteger)findMostPopularLetterInText:(NSString *)text
                            wihtAlphabet:(NSString *)alphabet;


@end
