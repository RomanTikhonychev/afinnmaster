//
//  TRFrequencyAnalyzer.m
//  VigenereMaster
//
//  Created by Рома on 2/23/16.
//  Copyright © 2016 Roman Tikhonychev. All rights reserved.
//

#import "TRFrequencyAnalyzer.h"

#import <math.h>

#define mod %
#define toString        [NSString alloc] initWithFormat:@"%i",

@implementation TRFrequencyAnalyzer

+ (NSDictionary *)calculateLetterFrequencyForText:(NSString *)text
                                     wihtAlphapet:(NSString *)alphabet
{
    
    NSMutableArray *alphabetLetters = [NSMutableArray new];
    
    int alphabetLenght = (int)[alphabet length];
    
    for (int i = 0; i < alphabetLenght; i++) {
        
        NSString *letter = [alphabet substringWithRange:NSMakeRange(i, 1)];
        [alphabetLetters addObject:letter];
    }
    

    NSMutableArray *alphabetLettersFrequency = [NSMutableArray new];
    
    for (NSString *letter in alphabetLetters) {
        
        NSRange searchRange =  NSMakeRange(0, [text length]);
        NSInteger counter = 0;
        
        while (YES) {
            
            NSRange range = [text rangeOfString:letter options:NSCaseInsensitiveSearch  range:searchRange];
            
            if (range.location != NSNotFound) {
                
                NSInteger index = range.location + 1;
                searchRange.location = index;
                searchRange.length = [text length] - index;
                counter++;
                
            } else {
                
                break;
            }
        }
        
        NSString *counterString = [NSString stringWithFormat:@"%ld", counter];
        [alphabetLettersFrequency addObject:counterString];
    }
    
    NSDictionary *letterFrequencyDictionary = @{ @"alphabetLetters"          : alphabetLetters,
                                                 @"alphabetLettersFrequency" : alphabetLettersFrequency};
    
    return letterFrequencyDictionary;
}


///warning calculateBigrammaFrequencyForText changed!
+ (int **)calculateBigrammaFrequencyForText:(NSString *)text
                                       wihtAlphapet:(NSString *)alphabet
{
     int textLenght     = (int)[text length];
     int alphabetLenght = (int)[alphabet length];
    
    //матрица количества вхождений
    int **matrica = (int **)malloc(alphabetLenght * sizeof(int *));
    
    for (int i = 0; i < alphabetLenght; i++)
        matrica[i] = (int *)malloc(alphabetLenght * sizeof(int));

    //fill with zero
    
    for (int i = 0; i < alphabetLenght; i++) {
        for (int k = 0; k < alphabetLenght; k++) {
            matrica[i][k] = 0;
        }
    }
    
    for (int a = 0; a < textLenght -2; a += 2)
    {
        int bigrammaX1 = ((int)[text characterAtIndex:a] - 1072) mod alphabetLenght;
        int bigrammaX2 = ((int)[text characterAtIndex:a +1] - 1072)    mod alphabetLenght;
        
        matrica[bigrammaX1][bigrammaX2]++;
    }
    
    //освобождаем память
//
//    for (int i = 0; i < alphabetLenght; i++)
//        free(matrica[i]);
//    free(matrica);
    
    return matrica;
}


+ (NSDictionary *)calculateBigrammaFrequencyANDReturnDictionaryForText:(NSString *)text
                                                          wihtAlphapet:(NSString *)alphabet
{
    int **frequencyMatrix = [TRFrequencyAnalyzer calculateBigrammaFrequencyForText:text wihtAlphapet:alphabet];
    
    NSMutableArray *alphabetLetters         = [NSMutableArray new];
    NSMutableArray *bigrammasArray          = [NSMutableArray new];
    NSMutableArray *bigrammasFrequencyArray = [NSMutableArray new];
    NSMutableDictionary *bigrammasFrequencyDictionary  = [NSMutableDictionary new];

    
    int alphabetLenght = (int)[alphabet length];
    
    for (int i = 0; i < alphabetLenght; i++) {
        
        NSString *letter = [alphabet substringWithRange:NSMakeRange(i, 1)];
        [alphabetLetters addObject:letter];
    }
    
    for (int i = 0; i < alphabetLenght; i++) {
        
        for (int k = 0; k < alphabetLenght; k++) {
            
            NSString *newBigramma = [NSString stringWithFormat:@"%@%@",alphabetLetters[i],alphabetLetters[k]];

            [bigrammasArray addObject:newBigramma];
            [bigrammasFrequencyArray addObject:[toString frequencyMatrix[i][k]]];
            [bigrammasFrequencyDictionary setObject:[toString frequencyMatrix[i][k]] forKey:newBigramma];
        }
    }
    
   NSArray *sorted = [bigrammasFrequencyDictionary keysSortedByValueUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
       return [obj2 integerValue] > [obj1 integerValue];
   }];
    
    NSMutableArray *maxValues           = [NSMutableArray new];
    NSMutableArray *maxValuesBigrammas  = [NSMutableArray new];
    
    for (int index = 0; index < 5; index++) {
        [maxValues addObject:[bigrammasFrequencyDictionary valueForKey:sorted[index]]];
        [maxValuesBigrammas addObject:sorted[index]];
    }
    
    NSDictionary *returnDictionary = @{@"max6Values"                    : maxValues,
                                       @"max6ValuesBigrammas"           : maxValuesBigrammas,
                                       @"bigrammasFrequencyDictionary"  : bigrammasFrequencyDictionary,
                                       @"bigrammasArray"                : bigrammasArray,
                                       @"bigrammasFrequencyArray"       : bigrammasFrequencyArray};
    
    return returnDictionary;
}


+ (CGFloat)calculateLetterEntropyForText:(NSString *)text
                            wihtAlphapet:(NSString *)alphabet
{
    
    NSDictionary *letterFrequencyDictionary = [TRFrequencyAnalyzer calculateLetterFrequencyForText:text wihtAlphapet:alphabet];
    
    NSArray *alphabetLetters             = [letterFrequencyDictionary objectForKey:@"alphabetLetters"];
    NSArray *alphabetLettersFrequency    = [letterFrequencyDictionary objectForKey:@"alphabetLettersFrequency"];
    
    NSInteger alphabetLettersLenght = [alphabetLetters count];
    NSInteger textLenght = [text length];
    CGFloat letterEntropy = 0;
    
    
    for (int i = 0; i < alphabetLettersLenght; i++) {

        CGFloat currentLetterReliability = ([[alphabetLettersFrequency objectAtIndex:i] floatValue] / textLenght);
        
        letterEntropy = letterEntropy + currentLetterReliability * log2(currentLetterReliability);
    }
    
    return -letterEntropy;
}


+ (CGFloat)calculateBigrammaEntropyForText:(NSString *)text
                              wihtAlphapet:(NSString *)alphabet
{

    int **bigrammasFrequencyMatrix = [TRFrequencyAnalyzer calculateBigrammaFrequencyForText:text
                                                                               wihtAlphapet:alphabet];

    int alphabetLenght      = (int)[alphabet length];
    CGFloat textLenght      = (CGFloat)[text length];
    
    CGFloat   bigrammaEntropy = 0;

    for (int i = 0; i < alphabetLenght; i++) {
        
        for (int j = 0; j < alphabetLenght; j++) {
            
            CGFloat currentBigrammaReliability = (((CGFloat)bigrammasFrequencyMatrix[i][j]) / textLenght);
            
            if (currentBigrammaReliability != 0) {
                
                CGFloat summElement = currentBigrammaReliability * log2(currentBigrammaReliability);
                bigrammaEntropy = bigrammaEntropy + summElement;
            }
        }
    }
    
    return -(bigrammaEntropy/2);
}


+ (NSInteger)findMostPopularLetterInText:(NSString *)text
                             wihtAlphabet:(NSString *)alphabet
{

    NSDictionary *letterFrequencyDictionary = [TRFrequencyAnalyzer calculateLetterFrequencyForText:text wihtAlphapet:alphabet];
    
    NSArray *alphabetLettersFrequency    = [letterFrequencyDictionary objectForKey:@"alphabetLettersFrequency"];
    
    NSInteger maxFrequencyLetterIndex   = -1;
    NSInteger maxFrequencyLetter        = -1;
    
    for (NSInteger i = 0; i < [alphabetLettersFrequency count]; i++) {
        
        NSString *frequencySring = alphabetLettersFrequency[i];
        
        if ([frequencySring integerValue] > maxFrequencyLetter) {
            
            maxFrequencyLetter = [frequencySring integerValue];
            maxFrequencyLetterIndex = i;
        }
    }
    
    return maxFrequencyLetterIndex;
}

@end





