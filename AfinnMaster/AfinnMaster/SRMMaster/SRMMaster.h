//
//  SRMMaster.h
//  AfinnMaster
//
//  Created by Рома on 3/13/16.
//  Copyright © 2016 Roman Tikhonychev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SRMMaster : NSObject


+ (NSString *)getConversedValueFor:(int)value withModul:(int)module;

+ (int)greatestCommonDivisorForValues:(int)firstValue seconValue:(int)secondValue;

/// ax = b(mod n)
+ (NSString *)solveLinearComparisonForValueA:(int)valueA valueB:(int)valueB valueN:(int)valueN;

@end
