//
//  SRMMaster.m
//  AfinnMaster
//
//  Created by Рома on 3/13/16.
//  Copyright © 2016 Roman Tikhonychev. All rights reserved.
//

#import "SRMMaster.h"

#define mod %
#define toString        [NSString alloc] initWithFormat:@"%i",
#define toStringComma   [NSString alloc] initWithFormat:@",%i",
#define RussianAlphabetEEHardSign @"абвгдежзийклмнопрстуфхцчшщыьэюя"

@implementation SRMMaster


+ (NSString *)getConversedValueFor:(int)value withModul:(int)module
{
    int check = [SRMMaster greatestCommonDivisorForValues:value seconValue:module];
    NSString *answer = @"Values GCD not equal to 1";
    
    if (check == 1) {
        
        NSMutableArray *arrayRi = [[NSMutableArray alloc] init];
        NSMutableArray *arrayQi = [[NSMutableArray alloc] init];
        
        int r0,r1;
        int r2 = 0;
        
        if (value > module) {
            r0 = value;
            r1 = module;
        } else {
            r0 = module;
            r1 = value;
        }
        
        [arrayRi addObject:[toString r0]];
        [arrayRi addObject:[toString r1]];
        
        while (r1) {
            
            r2 = r0 mod r1;
            
            [arrayRi addObject:[toString r2]];
            
            if (r1 != 0) {
                [arrayQi addObject:[toString (r0 - r2)/r1]];
            }
            
            r0 = r1;
            r1 = r2;
        }
        
        
        int u0 = 1;
        int u1 = 0;
        int ui = 0;
        
        int v0 = 0;
        int v1 = 1;
        int vi = 0;
        
        for (int index = 0; index < [arrayQi count]; index++) {
            
            int qi = (int)[[arrayQi objectAtIndex:index] integerValue];
            
            ui = u0 - (qi * u1);
            u0 = u1;
            u1 = ui;
            
            vi = v0 - (qi * v1);
            v0 = v1;
            v1 = vi;
        }
        
        if (v0 < 0) {
            v0 = v0 + ((value > module) ? value : module);
        }
        
        answer = [toString v0];
    }
    
    return answer;
}


+ (int)greatestCommonDivisorForValues:(int)firstValue seconValue:(int)secondValue
{
    int r0 = firstValue;
    int r1 = secondValue;
    int r2 = 0;

    while (r1) {
        r2 = r0 mod r1;
        r0 = r1;
        r1 = r2;
    }
    return r0;
}


+ (NSString *)solveLinearComparisonForValueA:(int)valueA valueB:(int)valueB valueN:(int)valueN
{
    NSString *answer = @"Linear comparison has no solutions";
    
    int gcdAN = [SRMMaster greatestCommonDivisorForValues:valueA seconValue:valueN];
    
    if (gcdAN == 1) {
        
        int convertA = (int)[[SRMMaster getConversedValueFor:valueA withModul:valueN] integerValue];
        answer = [toString ((convertA * valueB) mod valueN)];
        
    } else if ((valueB mod gcdAN) == 0 ) {
        
        int a1 = valueA / gcdAN;
        int b1 = valueB / gcdAN;
        int n1 = valueN / gcdAN;
        
        int x0 = (int)[[SRMMaster solveLinearComparisonForValueA:a1 valueB:b1 valueN:n1] integerValue];
        
        answer = [toString x0];
        
        for (int index = 1; index < gcdAN; index++) {

            int xi = x0 + index*n1;
            answer = [answer stringByAppendingString:[toStringComma xi]];
        }
        
    }
    
//    if ([answer isEqualToString:@"Linear comparison has no solutions"]) {
//        NSLog(@"problem");
//    }
    
    return answer;
}


@end







