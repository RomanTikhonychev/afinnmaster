//
//  TextRecognizer.m
//  AfinnMaster
//
//  Created by Рома on 3/14/16.
//  Copyright © 2016 Roman Tikhonychev. All rights reserved.
//

#import "TextRecognizer.h"
#import "TRIndexManager.h"

#define RussianAlphabetEEHardSign @"абвгдежзийклмнопрстуфхцчшщыьэюя"

@implementation TextRecognizer

+ (BOOL)isRussianLanguageText:(NSString  *)text
{
    BOOL answer = YES;
    
    
    NSArray *unRussianBigramms = @[@"ыы",@"ээ",@"щщ",@"шш",@"ьь",@"ьы",@"ыь"];
    
    for (NSString *bigrm in unRussianBigramms) {
       
        if ([TextRecognizer text:text containString:bigrm]) {
            
            answer = NO;
        }
    }
    

    
    
    
    CGFloat index = [TRIndexManager countIndexOfAccordanceForBlockOfText:text withAplhabet:RussianAlphabetEEHardSign];
    
    if (index < 0.047) {
        answer = NO;
    }
    
    if (index > 0.9) {
        answer = NO;
    }
    
    return answer;
}


+ (BOOL)text:(NSString *)text containString:(NSString *)string
{
    int stringLenght = (int)[string length];
    for (int index = 0; index - stringLenght; index++) {
        
        //2
        if ([text characterAtIndex:index] == [string characterAtIndex:0]
            && [text characterAtIndex:index + 1] == [string characterAtIndex:1]) {
            
            return YES;
        }
        
    
    }

    return NO;
}

@end










