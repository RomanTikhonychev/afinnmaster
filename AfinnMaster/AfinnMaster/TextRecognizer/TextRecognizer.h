//
//  TextRecognizer.h
//  AfinnMaster
//
//  Created by Рома on 3/14/16.
//  Copyright © 2016 Roman Tikhonychev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TextRecognizer : NSObject

+ (BOOL)isRussianLanguageText:(NSString  *)text;

@end
